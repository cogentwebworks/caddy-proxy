## very small caddy-proxy container but still have a working s6-overlay process and socklog-overlay


[![Docker Pulls](https://img.shields.io/docker/pulls/cogentwebs/caddy-proxy.svg)](https://hub.docker.com/r/cogentwebs/caddy-proxy/)
[![Docker Stars](https://img.shields.io/docker/stars/cogentwebs/caddy-proxy.svg)](https://hub.docker.com/r/cogentwebs/caddy-proxy/)
[![Docker Build](https://img.shields.io/docker/automated/cogentwebs/caddy-proxy.svg)](https://hub.docker.com/r/cogentwebs/caddy-proxy/)
[![Docker Build Status](https://img.shields.io/docker/build/cogentwebs/caddy-proxy.svg)](https://hub.docker.com/r/cogentwebs/caddy-proxy/)

This is a very small caddy-proxy container but still have a working s6-overlay process and socklog-overlay .